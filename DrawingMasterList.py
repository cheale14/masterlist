import socket # tcp socket stsuff
import sys
import os
import threading
import datetime
import time
import operator # idk
import hashlib
import smtplib
import traceback # exception/error handling
from urllib.parse import unquote # http request parsing (eg, "%20" -> " ")
from pathlib import Path
MASTERLIST_SCRIPT_VERSION=14
# --------------------- NOTICE ----------------------
"""
WARNING: THIS MASTERLIST SCRIPT VERSION NEEDS
TO BE CHANGED BETWEEN VERSIONS
THE PI WILL CHECK THE masterlist_version.txt FILE
TO SEE IF AN UPDATE HAS BEEN MADE
YOU WILL NEED TO RUN THE SCRIPT WITH AN UPDATED VERSION ATLEAST ONCE
"""
# --------------------- NOTICE ----------------------

pyTerminal = "-- TERMINAL OUTPUT: MasterList - V" + str(MASTERLIST_SCRIPT_VERSION) + " -- "
def prt(*args):
    global pyTerminal
    # I may change the way print works in the future
    # So that it logs it
    # And so that it handles multi-thread better
    # eg, currently it will like.. write ontop of each other
    _now = datetime.datetime.now()
    datestring = _now.strftime("[%H:%M:%S.%f]")
    str_ = datestring + " "
    for i in args:
        str_ += str(i) + " "
    print(str_)
    logPath = os.path.dirname(os.path.realpath(__file__))
    logPath = logPath + "/logs/" 
    path = Path(logPath)
    if not path.exists():
        os.mkdir(logPath)
    logPath = logPath + _now.strftime("%d_%m_%y") + ".txt"
    path = Path(logPath)
    with open(logPath, "a+") as file:
        file.write(str_ + "\r\n")
    pyTerminal += "\r\n" + str_
    
prt("-- Starting MasterList running version", str(MASTERLIST_SCRIPT_VERSION))
def get_ip():
    # External IP
    servSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        servSocket.connect(('10.255.255.255', 1))
        IP = servSocket.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        servSocket.close()
    return IP
 
HOST = str(get_ip())   # Symbolic name meaning all available interfaces
PORT = 8889 # Arbitrary non-privileged port
SERVERS = []
TOP_LEADERBOARD = {}
ADMIN_LOCATION = ""

today = datetime.date.today()
hash = hashlib.sha256(bytes(str(today), "utf-8")).hexdigest()
ADMIN_LOCATION = str(hash)[:6]

with open("config.txt", "r") as file:
    email = file.readline()
    passwd = file.readline()
    recip = file.readline()
    try:  
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(email, passwd)
        msg = "Subject: Masterlist Admin Page\n\n"
        msg += "\r\nMasterlist admin location: " + ADMIN_LOCATION 
        msg += "\r\nOutput: masterlist.ddns.net:8889/" + ADMIN_LOCATION + "/output/"
        msg += "\r\nReset scores: masterlist.ddns.net:8889/" + ADMIN_LOCATION + "/reset_scores/"
        msg += "\r\nRecheck servers: masterlist.ddns.net:8889/" + ADMIN_LOCATION + "/recheck/" 
        server.sendmail(email, recip, msg)
    except Exception as e:  
        prt("Error while attempting to eee", e)

class DrawingServer():
    # Holds the name, IP, players and online status of a server
    def __init__(self, name, internalIP, externalIP, players, runExternal, tcpConn):
        self.Name = str(name)
        self.ExtIP = externalIP
        self.IntIP = internalIP
        self.External = runExternal
        if runExternal or externalIP != internalIP:
            self.ConnIP = str(self.ExtIP)
        else:
            self.ConnIP = str(self.IntIP)
        self.PlayerCount = str(players)
        self.Online = True
        self.Conn = tcpConn
        self.LastUpdate = datetime.datetime.now()
        prt("New server, info:")
        prt("Name:", self.Name)
        prt("ExIP:", self.ExtIP)
        prt("IntIP:", self.IntIP)
        prt("ConnIP:", self.ConnIP)
    def StillRunning(self):
        # Notifies that this server is still running
        self.Online = True
        self.LastUpdate = datetime.datetime.now()
    def IsRunning(self):
        # Boolean, also checks to see if it should be removed
        current = datetime.datetime.now()
        diff = current - self.LastUpdate
        timeDiff = diff.seconds
        if (timeDiff > (5 * 60)) and self.Online:
            self.Close()
            return False
        prt(self.Name, "server, last checkin", timeDiff, " secs ago")
        return self.Online
    def Close(self):
        self.Online = False
        prt("Server registered offline: ", str(self))
        SERVERS.remove(self)
    def TimeLeft(self):
        # Time until next checkin from the server is expected
        current = datetime.datetime.now()
        diff = current - self.LastUpdate
        timeDiff = diff.seconds
        return str((5 * 60) - timeDiff)
    def __str__(self):
        self.IsRunning()
        return str(self.Name) + " [" + str(self.Online) + "] | " + str(self.ConnIP) + " (" + str(self.PlayerCount) + "/32) - " + self.TimeLeft()
    def __unicode__(self):
        self.IsRunning()
        return str(self.Name) + " [" + str(self.Online) + "] | " + str(self.ConnIP) + " (" + str(self.PlayerCount) + "/32) - " + self.TimeLeft()
    def __repr__(self):
        self.IsRunning()
        return str(self.Name) + " [" + str(self.Online) + "] | " + str(self.ConnIP) + " (" + str(self.PlayerCount) + "/32) - " + self.TimeLeft()
    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return self.__dict__ == other.__dict__
        return False
        
def GetServer(name):
    toReturn = None
    for serv in SERVERS:
        if serv.Name == name:
            toReturn = serv
            break
    return toReturn

class myThread (threading.Thread):
    # Handles the client input/request
    def __init__(self, threadID, name, counter, connection, address):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
        self.conn = connection
        self.addr = address
    def run(self):
        try:
            clientthread(self.conn, self.addr)
        except Exception as e:
            try:
                conn.sendall(bytes("500", "utf-8"))
            except:
                pass
            raise
        prt(self.name, " thread closed")

class LoopThread (threading.Thread):
    # Thread for saving leaderboard every x mins
    def __init__(self, threadID):
        threading.Thread.__init__(self)
    def run(self):
        prt("Starting save info loop..")
        saveLoop()

# Returns string
# String is the stuff between <body>[HERE]</body>
def HandleHTTPPath(path, conn):
    global SERVERS, TOP_LEADERBOARD
    response = "<p><strong>500</strong><br />An internal error has occured while trying to access: <strong>" + path + "</strong></p>"
    try:
        path = unquote(path)
        pathSplit = path.split("/")
        pathSplit.pop(0)
        # expected sort of thing:
        # /server/name/TestServerName
        # /server/ip/10.247.10.2
        # to get server info, or..
        # /help/
        # for it to display help on how it works
        # anything else is 400 (bad request)
        if pathSplit[0] == "server":
            # getting server info, by..
            serv = None
            if pathSplit[1] == "name":
                # name
                try:
                    serv = GetServer(pathSplit[2])
                except:
                    pass
            elif pathSplit[1] == "ip":
                # ip
                for s in SERVERS:
                    if s.ConnIP == pathSplit[2]:
                        serv = s
                        break
            else:
                response = "<strong>400</strong><br />Unknown get server type: <em>" + pathSplit[1] + "</em></strong>"
                return
            if serv is None:
                response = "<strong>404</strong><br /><em>Specified server not found</em>"
            else:
                resSplit = ["<strong>" + serv.Name + "</strong>"]
                resSplit.append("<strong>Players online:</strong> " + serv.PlayerCount)
                resSplit.append("<strong>IP:</strong> " + serv.ConnIP)
                if not serv.IsRunning():
                    response = "<strong>410</strong><br/><em>Not found - That server is no longer here.</em>"
                    return
                resSplit.append("<strong>Next Refresh Due:</strong> " + str(serv.TimeLeft()) + "s")
                resSplit.append("<br><a href=\"/../../details/" + serv.Name + "\">Details</a>")
                resSplit.append("<br /><a href=\"/../../\">Return</a>")
                response = "<br />".join(resSplit)
        elif pathSplit[0] == "help":
            # display help
            resSplit = []
            resSplit.append("<h1>Masterlist Web Commands</h1>")
            resSplit.append("<h3>Get Server Info</h3><em>website.com</em>/server/<strong>option</strong>/<strong>value</strong>"
                            "<br/>The option can be <em>name</em> or <em>ip</em>"
                            "<br/>Value is either the name or IP of server, depending on the option"
                            "<br/><em>This will return information regarding the specific server</em><br/>")
            resSplit.append("<h1>MasterList Socket Commands</h1>")
            resSplit.append("<strong>POST:<em>[Name]</em>;<em>[IP]</em>;<em>[PlayerCount]</em></strong><br/>Notifies the masterlist a server is online<br/>Returns: 409 if masterlist already has server" "<br/>")
            resSplit.append("<strong>REFRESH:<em>Name</em></strong><br/>Informs the Masterlist that the givne server is still running<br/>Returns: none" "<br/>")
            resSplit.append("<strong>UPDATE:<em>[Name]</em>;<em>[IP]</em>;<em>[PlayerCount]</em></strong><br/>Updates masterlist info on a server<br/>Returns: none" "<br/>")
            resSplit.append("<strong>GET_SERVERS</strong><br/>Returns: <strong>-</strong> separated list of servers, in format: <em>name</em>;<em>ip</em>;<em>players</em>" "<br/>")
            resSplit.append("<strong>GET_SCORES</strong><br/>Returns: <strong>-</strong> separated list of leaderboard, in format: <em>name</em>:<em>score</em>" "<br/>")
            resSplit.append("<strong>ADDSCORE:<em>[PlayerName]</em></strong><br/>Adds one score to a user, in the leaderboard.")
            response = "".join(resSplit)
        elif pathSplit[0] == ADMIN_LOCATION:
            if pathSplit[1] == "output":
                resSplit = []
                resSplit.append("<p><strong>Terminal Output:</strong></p><br>")
                resSplit.append("<pre>" + pyTerminal + "</pre>")
                response = "".join(resSplit)
            elif pathSplit[1] == "reset_scores":
                TOP_LEADERBOARD = {}
                response = "Done"
            elif pathSplit[1] == "recheck":
                temp = str(SERVERS)
                response = "Done:<br>"+ str(SERVERS)
        elif pathSplit[0] == "details":
            if len(pathSplit) == 2:
                servName = pathSplit[1]
                serv = GetServer(servName)
                if serv is None:
                    response = "404"
                else:
                    try:
                        serv.Conn.sendall(bytes("GetDetails", "utf-8"))
                    except Exception as ex:
                        print("'" + str(ex) + "'", "An existing connection was forcibly closed by the remote host" in str(ex))
                        if "An existing connection was forcibly closed by the remote host" in str(ex):
                            serv.Close()
                            response = "500\r\nThat server did not respond to the masterlist and has been noted as offline."
                            return
                        else:
                            prt(ex)
                            return "500\r\nError encountered while attempting to communicate with that server - it may be offline or busy"
                    servResponse = serv.Conn.recv(2048)
                    serv.StillRunning()
                    servResponse = servResponse.decode("utf-8")
                    items = servResponse.split(",")
                    if servResponse is None or len(servResponse) < 2:
                        response = "<p>Unable to contact server, or no players</p>"
                    elif servResponse == "[None]":
                        response = "<p>Server reports no players are online</p>"
                    else:
                        resSplit = []
                        
                        players = items[3].split(";")
                        prt(len(players))
                        if len(players) > 1: # 1, because the trailing slash makes it so
                            resSplit.append("<table><tr><th>Players</th><th>Score</th></tr>")
                            for player in players:
                                if len(player) == 0:
                                    continue
                                name = player.split(":")[0]
                                score = player.split(":")[1]
                                resSplit.append("<tr><td>" + name + "</td><td>" + score + "</td></tr>")
                            resSplit.append("</table>")
                            resSplit.append("<br>")
                            serv.PlayerCount = len(players) - 1
                        else:
                            resSplit.append("<p>No players online</p>")
                        if items[0] == "True":
                            resSplit.append("<p>Status: In Lobby</p>")
                        else:
                            resSplit.append("<p>Status: In game</p>")
                            resSplit.append("<p>Painter: " + items[1] + "</p>")
                            resSplit.append("<p>Item: " + items[2] + "</p>")
                        response = "".join(resSplit)
            else:
                response = "400"
        else:
            response = "<strong>404</strong><br /><em>Unable to locate item " + path + "</em>"
    except Exception as e:
        prt("HERE", e)
        traceback.print_exc()
        raise
    finally:
        return response

servSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
 
#Bind socket to local host and port
try:
    servSocket.bind((HOST, PORT))
except socket.error as msg:
    prt("Bind failed. Error Code : " + str(msg[0]) + " Message " + msg[1])
    sys.exit()
     
prt("Socket bind complete")
 
#Start listening on socket
servSocket.listen(10)
prt("Socket now listening:", str(HOST) + ":" + str(PORT))
 
#Function for handling connections. This will be used to create threads
def clientthread(conn, address):
    global SERVERS
    #infinite loop so that function do not terminate and thread do not end.
    SelfIP = address[0]
    SelfConnStr = str(address[0]) + ":" + str(address[1])
    prt("Started info with", address)
    while True:
        #Receiving from client
        data = conn.recv(1024)
        if not data: 
            break
        data = data.decode("utf-8")
        if len(data) > 600:
            conn.sendall(bytes("413", "utf-8"))
            continue
        if len(SERVERS) > 10:
            conn.sendall(bytes("429", "utf-8"))
            continue
        prt(SelfConnStr + ":", data)
        returnMsg = "Error"
        if data.startswith("POST:"):
            # Adds a server to masterlist
            data = data[5:]
            try:
                # Expected format is:
                # Name;IP;PlayerCount
                splitInfo = data.split(';')
                sName = splitInfo[0].strip()
                sIntIP = splitInfo[1].strip()
                sExtIP = splitInfo[2].strip()
                sPlayers = splitInfo[3].strip()
                sExternal = splitInfo[4].strip()
                newServer = DrawingServer(sName, sIntIP, sExtIP, sPlayers, sExternal, conn)
                hostedAlready = False
                if newServer in SERVERS:
                    hostedAlready = True
                else:
                    for serv in SERVERS:
                        if serv.Name == newServer.Name:
                            hostedAlready = True
                            break
                        elif serv.ConnIP == newServer.ConnIP:
                            hostedAlready = True
                            break
                if hostedAlready:
                    returnMsg = "409"
                    prt("Duplicate server attempted:", newServer.Name, SERVERS)
                else:
                    SERVERS.append(newServer)
                    prt("New server added:", newServer)
                    returnMsg = "200"
            except Exception as msg:
                raise
            prt(data)
        elif data.startswith("REFRESH:"):
            # Tells masterlist the server is still online
            data = data[8:]
            returnMsg = "500"
            try:
                serv = GetServer(data)
                if serv is None:
                    returnMsg = "404"
                else:
                    if serv.ExtIP == SelfIP:
                        serv.StillRunning()
                        returnMsg = "200"
                    else:
                        returnMsg = "403"
            except:
                raise
        elif data.startswith("UPDATE:"):
            # Updates server stats (eg: players)
            # This doesnt technically work
            # This cant listen past the first message
            data = data[7:]
            try:
                splitInfo = data.split(';')
                newServer = DrawingServer(splitInfo[0].strip(), splitInfo[1].strip(), splitInfo[2].strip())
                oldServer = GetServer(newServer.Name)
                if oldServer is None:
                    returnMsg = "404"
                else:
                    if serv.ConnExIP == SelfIP:
                        index = SERVERS.index(oldServer)
                        SERVERS[index] = newServer
                        returnMsg = "200"
                    else:
                        returnMsg = "403"
            except:
                raise
        elif data == "GET_SERVERS":
            # Gets readable list of servers
            temp = str(SERVERS) # Refreshes list (removes offline servers)
            toBeServers = []
            for serv in SERVERS:
                toBeServers.append(serv.Name + ";" + serv.ConnIP + ";" + serv.PlayerCount)                
            returnMsg = "200.1:" + "-".join(toBeServers)
        elif data == "GET_SCORES":
            # Returns parseable format of leaderboard
            toBeThing = []
            for key, value in TOP_LEADERBOARD.items():
                toBeThing.append(key + ":" + str(value))
            returnMsg = "200.2:" + "-".join(toBeThing)
        elif data.startswith("ADDSCORE:"):
            # For a server, though technically anyone can..
            # This doesnt technically works
            # This cannot listen after the first message sent
            data = data[len("ADDSCORE:"):]
            TOP_LEADERBOARD[data] = TOP_LEADERBOARD.get(data, 0) + 1
            returnMsg = "200"
        else:
            # Anything else is an unknown request
            if "HTTP" in data:
                # http request starts with.. "GET / HTTP" where / is the path
                response_body = []
                response_body.append("<!DOCTYPE html>")
                response_body.append("<html>")
                response_body.append("<head>")
                response_body.append("<style>")
                response_body.append(" table, th, td { border: 1px solid black; }")
                response_body.append("</style>")
                response_body.append("<title>Drawing Masterlist</title>")
                response_body.append("</head>")
                response_body.append("<body>")
                # Here we decide if we handle it with a generic, or
                # see if they requested a more.. specific location
                dataSplit = data.split('\r\n')
                getIndex = dataSplit[0].index("GET ") + len("GET ")
                httpIndex = dataSplit[0].index(" HTTP/")
                requestPath = dataSplit[0][getIndex:httpIndex]
                if requestPath == "/":
                    # / is the main page, so generic server list & leaderboard
                    if len(SERVERS) > 0:
                        response_body.append("<table>")
                        response_body.append("<tr><th>Server Name</th><th>Player Count</th></tr>")
                        for serv in SERVERS:
                            servName = "<a href=\"/server/name/" + serv.Name + "\">" + serv.Name + "</a>"
                            response_body.append("<tr><td><strong>" + servName + "</strong></td><td>" + serv.PlayerCount + "</td></tr>")
                        response_body.append("</table>&nbsp;")
                    if len(TOP_LEADERBOARD) > 0:
                        response_body.append("<p>Current Leaderboard:</p>")
                        response_body.append("<table>")
                        response_body.append("<tr><th>Player Name</th><th>Score</th></tr>")
                        for name, val in TOP_LEADERBOARD.items():
                            response_body.append("<tr><td><strong>" + name + "</strong></td><td>" + str(val) + "</td></tr>")
                        response_body.append("</table>")
                    if len(TOP_LEADERBOARD) == 0 and len(SERVERS) == 0:
                        response_body.append("<p>Page successfully loaded, but there is no information to display.</p>")
                else:
                    response_body.append(HandleHTTPPath(requestPath, conn))
                response_body.append("<br /><br /><p><em>MasterList Version: " + str(MASTERLIST_SCRIPT_VERSION) + "</em> - "
                                     " <em><a href=\"/help/\">View Help</a></em> - "
                                     " <em><a href=\"/\">Main Screen</a></em></p>")
                response_body.append('</body></html>')
                response_body_raw = ''.join(response_body)

                    # Clearly state that connection will be closed after this response,
                    # and specify length of response body
                response_headers = {
                        'Content-Type': 'text/html; encoding=utf8',
                        'Content-Length': len(response_body_raw),
                        'Connection': 'close',
                    }
                # http response headers

                response_headers_raw = ''.join('%servSocket: %servSocket\n' % (k, v) for k, v in \
                                                            response_headers.items())
                    # Reply as HTTP/1.1 server, saying "HTTP OK" (code 200).
                response_proto = 'HTTP/1.1'
                response_status = '200'
                response_status_text = 'OK'

                    # sending all this stuff
                conn.send(bytes('%servSocket %servSocket %servSocket' % (response_proto, response_status, \
                                                                    response_status_text), "utf-8"))
                conn.send(bytes(response_headers_raw, "utf-8"))
                conn.send(bytes('\n', "utf-8")) # to separate headers from body
                conn.send(bytes(response_body_raw, "utf-8"))
                conn.close()
                return
            else:
                returnMsg = "501 - I dont know what you want me to do" # 501 = not implemented, we dont know what they requested means
        conn.sendall(bytes(returnMsg, "utf-8"))
        break
    #came out of loop
    #prt("Connection from", conn.getsockname()[0], "closed")
    #conn.close()
    # dont want to close the socket so it can be kept in list
    # and so it masterlist can request things
    while True:
        pass # need to keep thread alive
def saveLoop():
    global TOP_LEADERBOARD
    try:
        with open("scores.txt", "r") as file:
            for line in file:
                lineSplit = line.split(" ")
                name = lineSplit[0].replace("_", " ")
                value = lineSplit[1]
                TOP_LEADERBOARD[name] = int(value)
        TOP_LEADERBOARD = dict(sorted(TOP_LEADERBOARD.items(), key=operator.itemgetter(1), reverse=True))
        prt("Leaderboard:", TOP_LEADERBOARD)
    except FileNotFoundError:
        pass
    except:
        raise
    while True:
        with open("scores.txt", "w") as file:
            for key, value in TOP_LEADERBOARD.items():
                file.write(key.replace(" ", "_") + " " +  str(value) + "\n")
        time.sleep(60*5)

saveLop = LoopThread(1)
saveLop.start()

with open("masterlist_version.txt", "w") as file:
    file.write(str(MASTERLIST_SCRIPT_VERSION))
prt("Admin path:", "/" + ADMIN_LOCATION + "/")
while 1:
    #wait to accept a connection - blocking call
    conn, addr = servSocket.accept()
    prt("Connected with " + addr[0] + ":" + str(addr[1]))
    newThread = myThread(1, str(addr[0]), 1, conn, addr)
    newThread.start()

servSocket.close()
